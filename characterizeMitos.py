# IMAGE ANALYSIS: ARE MITOCHONDRIA IN CELL FUSED OR FISSED?
#
# - per hand: mark each cell as separate region of interest (ROI)
#	- with Freehand-Tool frame each cell + press "t"
# 
# - Each ROI is then treated with some filtering steps in order to extract 
#   the best suitable skeleton. Here, two steps are adjustable by changing 
#   parameters (see below).
#	
# - Measures per ROI: 
#		- number_of_trees/number_of_branches: 
#				< 0.6: trees have many branches, mitochondria are fused 
#				> 0.95: many trees have only on or zero branch (=dot), mitochondria are fragmented
#		- average Aspect Ratio: longest dilation(major axis)/shortest dilation (minor axis), = 1 would mean major axis equals minor axis
#				< 2: structures are rather round, mitochondria are fragmented and swollen
#
#
#### SET FILTERING PARAMETERS #####
#
# parameters of background substraction (radius) and sharpening (radius)
# can be changed to influence outcome
#	
# ### background substraction: ###
# Everything gets substracted, where a ball with certain size (e.g. radius=20 pixel) can reach
# Structures larger than this radius will vanish.
#
# ### sharpening ###
# Exaggerate edges of remaining structures after background substraction 
# 2 parameters: 
# 	radius: influences thickness of resulting structure
# 	mask: "steepness" of edges, i.e. contrast between structure and background,
# 		  highest possible value (=0.9) recommended 

###########################################################

backgroundRadius = 6  # radius in microns
sharpeningRadius = 0.7  # radius in microns	

###########################################################

from ij import IJ
import os
from ij.gui import WaitForUserDialog
from ij.io import DirectoryChooser
from ij.plugin.frame import RoiManager
from ij.measure import Measurements as Measurements
from ij.process import ImageStatistics as ImageStatistics
from ij.plugin.frame import RoiManager
from ij.plugin.filter import ParticleAnalyzer
from ij.measure import ResultsTable
from sc.fiji.analyzeSkeleton import AnalyzeSkeleton_
from loci.plugins import BF


def AnalyzeROIs(imp1, roi_manager, filename, backgroundRadius, sharpeningRadius):
	roi_list = roi_manager.getRoisAsArray()

	results = [[0,0,0,0,0] for _ in range(len(roi_list))] # table to store results in
	# results = [[ cell1, cell2, ...], [area, number of branches, total length of branches]]
	
	# stack manipulation:
	# if image is stack with more than 1 image (e.g. channel with TopoA) delete last image
	if imp1.getStackSize != 1:
		stack = imp1.getImageStack()
		stack.deleteLastSlice()
		imp1 = stack
		

	# loop over ROIs
	for roi in range(len(roi_list)):	
		
		# set focus on current image
		imp1 = IJ.getImage()
		# set current ROI on current image
		imp1.setRoi(roi_list[roi])
		# duplicate ROI only = new image
		imp = imp1.duplicate()
		imp.show()
		
		# initialize AnalyzeSkeleton
		skel = AnalyzeSkeleton_()
		skel.calculateShortestPath = True
		skel.setup("", imp)
		
		# initialize Particle Analyzer
		# Create a table to store the results of Particle Analyzer
		patable = ResultsTable()
		# Create a ParticleAnalyzer
		# Arguments: http://imagej.net/Jython_Scripting#..._and_counting_particles.2C_and_measuring_their_areas
		pa = ParticleAnalyzer(ParticleAnalyzer.SHOW_NONE, Measurements.CIRCULARITY | Measurements.PERIMETER | Measurements.ELLIPSE, patable, 0, float('inf'), 0.0, 1.0)
		
		# Image processing, filtering etc.
		# background substraction
		IJ.run(imp, "Subtract Background...", "rolling="+str(backgroundRadius)+" slice")
		# sharpen endges of remaining structures, 
		IJ.run(imp, "Unsharp Mask...", "radius="+str(sharpeningRadius)+" mask=0.90 slice")
		# perform reduction using threshold
		IJ.run(imp, "Make Binary", "slice")
		# reduce noise
		IJ.run(imp, "Despeckle", "slice")
		
		# Analyze particles
		pa.analyze(imp)
		
		# duplicate current binary image
		imppart = imp.duplicate()
		imppart.show()
		
		# get skeleton from image (structures are thinned until wideness of one pixel is reached) 
		IJ.run(imp, "Skeletonize (2D/3D)", "slice")
		# Analyze skeleton
		# run(int pruneIndex, boolean pruneEnds, boolean shortPath, ImagePlus origIP, boolean silent, boolean verbose)
		skelResult = skel.run(AnalyzeSkeleton_.NONE, False, True, None, True, False)
		
		#### Perform Skeleton analysis
		# Read the results
		branchLengths = skelResult.getAverageBranchLength()
		branchNumbers = skelResult.getBranches()
		numTrees = skelResult.getNumOfTrees()
		junctions = skelResult.getJunctions()
		# calculate total length (average branch length * branch numbers)
		totalLength = 0
		totalBranchNr = 0
		totJunctions = 0
		for i in range( len( branchNumbers)):
			totalLength += branchNumbers[i] * branchLengths[i]
			totalBranchNr += branchNumbers[i]		
			totJunctions += junctions[i]

		
		######## Perform Particle Analysis
		# Read Results
		AR = patable.getColumn(33) 		  # Aspect Ratio
		Perimeter = patable.getColumn(10) # Perimeter
		Major = patable.getColumn(15)	  # major axis	
		
		# Calculating Averages
		AvAspectRatio = 0
		AvPerimeter = 0
		for particle in range(len(Perimeter)):
			AvAspectRatio += AR[particle]
			AvPerimeter += Perimeter[particle]
		AvAspectRatio = AvAspectRatio/len(Perimeter)
		AvPerimeter = AvPerimeter/len(Perimeter)
		
			
		results[roi][1] = len(branchNumbers)
		results[roi][2] = totalBranchNr
		results[roi][3] = AvPerimeter
		results[roi][4] = AvAspectRatio
	
		# closes duplicate image w/o asking for saving
		imppart.changes = False
		imp.changes = False
		imppart.close()
		imp.close()
		
	# measures area of ROI 
	imp = IJ.getImage()

	for roi in range(len(roi_list)):
		imp.setRoi(roi_list[roi])
		stats = imp.getStatistics()
		results[roi][0] = stats.area
	
	# decide if ROI (cell) contains fused or  fragmented
	# save result to ResulTable
	for roi in range(len(roi_list)):
		# totalBranchNr / tree
		branchPerTree = float(results[roi][2])/results[roi][1] 	
		# Average Perimeter
		AVperimeter = results[roi][3]
		# Average Aspect Ratio
		AVAR = results[roi][4]
		
		# SCORES
		fragmented = 0
		fused = 0
		# many trees have more than 1 / only 1
		if branchPerTree > 1.1:
			fused +=1
		if branchPerTree > 1.2:
			fused +=2
		if branchPerTree < 1.0:
			fragmented +=1
		if branchPerTree < 0.9:
			fragmented +=1
		
		
		# perimeter is long / short
		if AVperimeter > 4:
			fused += 1
		if AVperimeter > 5:
			fused += 1
		if AVperimeter > 6:
			fused += 1
		if AVperimeter > 7:
			fused += 1
		if AVperimeter < 4:
			fragmented += 1
		if AVperimeter < 3.8:
			fragmented += 1
		
		# Aspect Ratio 
		if AVAR > 2.1:
			fused += 1
		if AVAR < 2.:
			fragmented += 1
		
		cell = 0
		if fused >= 4:
			cell = "fused"
		elif fragmented >= 4:
			cell = "fragmented"
		else:
			cell = "undecided" 
		
		# save result to ResultTable
		myfile = open(outputDir+"00_Results.txt", "a")
		myfile.write(filename+"\t"+str(roi+1)+"\t"+str(branchPerTree)+"\t"+str(AVperimeter)+"\t"+str(AVAR)+"\t"+cell+"\n")
		myfile.close()
		

# variables to open several images at once (in total 3) in order to compare		
imp2 = 0
imp3 = 0

RoiManager()
roi_manager = RoiManager.getInstance()

# prompts to get input and output directory
inputDir = DirectoryChooser("Please choose a directory to get images from.").getDirectory()
outputDir = DirectoryChooser("Please choose a directory to save results.").getDirectory()

if (not inputDir) | (not outputDir):
	# user canceled dialog
	pass


else:

	# create file to write results to
	myfile = open(outputDir + "00_Results.txt", "w")
	myfile.write("FILENAME\tROI IND\tBRANCHES PER TREE\tAV PERIMETER\tAV ASPECT RATIO\tSTATE\n")
	myfile.close()

	# loop over all files in output directory
	for root, dirs, files in os.walk(inputDir,topdown = False):
		for fileName in sorted(files):
			# open only files with ending lsm
			if fileName.split(".")[-1] == "lsm":  
				# open file & display
				imps = BF.openImagePlus(os.path.join(inputDir, fileName))
				for imp in imps:
					imp.show()
				# get filename for saving
				name=imp.getShortTitle()
				
				# delete last ROIs
				roi_manager.reset()
				# record current ROIs from user imput
				WaitForUserDialog("Action required", "Please mark each cell to be analyzed with a ROI: \nSurround each cell with the Freehand-Tool and press 't'. \nPress OK when done.").show()
				
				pixelwidth = imp.getCalibration().pixelWidth
				backgroundRInPixel = backgroundRadius/pixelwidth
				sharpeningRInPixel = sharpeningRadius/pixelwidth
				# analyze each ROI = cell
				if len(roi_manager.getRoisAsArray()) != 0:
					AnalyzeROIs(imp, roi_manager, name, backgroundRInPixel, sharpeningRInPixel)
				# show all ROIs in order to save them 
				roi_manager.runCommand("Show All")
				# save image plus ROIs as tif into output directory
				IJ.save(imp, os.path.join(outputDir, name + "-ROI.tif"))
				# close third-last image
				if imp3 != 0:
					imp3.close()
				if imp2 != 0:
					imp3 = imp2
				imp2 = imp
	# close all open images
	if imp2 != 0:
		imp2.close()
	if imp3 != 0:
		imp3.close()

	# SUMMARY: how many cells analyzed, save summary in in 00_Summary.txt
	fused = 0
	frag = 0
	swollen = 0
	undecided = 0

	# counting for summary
	lines = [line.rstrip('\n') for line in open(outputDir + "00_Results.txt")]
	for line in lines:
		if line.split("\t")[-1] == "fused":
			fused += 1	
		if line.split("\t")[-1] == "fragmented":
			frag += 1
		if line.split("\t")[-1] == "undecided":
			undecided += 1

	myfile = open(outputDir + "00_Summary.txt", "w") 
	myfile.write("analyzed cells: "+str(len(lines)-1)+"\ncells w fused mitochondria: "+str(fused)+"\ncells w fragmented mitochondria: "+str(frag)+"\ncells w no clear phenotype: "+str(undecided))
	myfile.close()




# IMAGE ANALYSIS: ARE MITOCHONDRIA IN CELL FUSED OR FISSED?
#
# - per hand: mark each cell as separate region of interest (ROI)
#	- with Freehand-Tool frame each cell + press "t"
# 
# - Each ROI is then treated with some filtering steps in order to extract 
#   the best suitable skeleton. Here, two steps are adjustable by changing 
#   parameters (see below).
#	
# - Measures per ROI: 
#		- number_of_trees/number_of_branches: 
#				< 0.6: trees have many branches, mitochondria are fused 
#				> 0.95: many trees have only on or zero branch (=dot), mitochondria are fragmented
#		- average Aspect Ratio: longest dilation(major axis)/shortest dilation (minor axis), = 1 would mean major axis equals minor axis
#				< 2: structures are rather round, mitochondria are fragmented and swollen
#
#
#### SET FILTERING PARAMETERS #####
#
# parameters of background substraction (radius) and sharpening (radius)
# can be changed to influence outcome
#	
# ### background substraction: ###
# Everything gets substracted, where a ball with certain size (e.g. radius=20 pixel) can reach
# Structures larger than this radius will vanish.
#
# ### sharpening ###
# Exaggerate edges of remaining structures after background substraction 
# 2 parameters: 
# 	radius: influences thickness of resulting structure
# 	mask: "steepness" of edges, i.e. contrast between structure and background,
# 		  highest possible value (=0.9) recommended 

###########################################################

backgroundRadius = 6  # radius in um
sharpeningRadius = 0.7  # radius in um

###########################################################

from ij import IJ
import os
from ij.gui import WaitForUserDialog
from ij.io import DirectoryChooser
from ij.plugin.frame import RoiManager
from ij.measure import Measurements as Measurements
from ij.process import ImageStatistics as ImageStatistics
from ij.plugin.frame import RoiManager
from ij.plugin.filter import ParticleAnalyzer
from ij.measure import ResultsTable
from sc.fiji.analyzeSkeleton import AnalyzeSkeleton_
from loci.plugins import BF
from ij.plugin.filter import Analyzer

#############################################################
def AnalyzeROIs(imp1, roi_manager, filename, backgroundRadius, sharpeningRadius):

	roi_list = roi_manager.getRoisAsArray()
	results = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] for _ in range(len(roi_list))] # table to store results in
	# results = [[ cell1, cell2, ...], [area, number of trees, total length of branches, distribution of branch lengths, ...]]
	imp1 = IJ.getImage()
	# stack manipulation:
	# if image is stack with more than 1 image (e.g. channel with TopoA) delete last image
	if imp1.getStackSize != 1:
		stack = imp1.getImageStack()
		stack.deleteLastSlice()
		imp1 = stack

		# loop over ROIs
	for roi in range(len(roi_list)):	
		
		# set focus on current image
		imp1 = IJ.getImage()
		# set current ROI on current image
		imp1.setRoi(roi_list[roi])
		# duplicate ROI only = new image
		imp = imp1.duplicate()
		imp.show()
		
		# initialize Particle Analyzer
		# Create a table to store the results of Particle Analyzer
		patable = ResultsTable()
		# Create a ParticleAnalyzer
		# Arguments: http://imagej.net/Jython_Scripting#..._and_counting_particles.2C_and_measuring_their_areas
		pa = ParticleAnalyzer(ParticleAnalyzer.SHOW_NONE, Measurements.CIRCULARITY| Measurements.PERIMETER | Measurements.ELLIPSE, patable, 0, float('inf'), 0.0, 1.0)

		# initialize AnalyzeSkeleton
		skel.setup("", imp)
		
		# Image processing, filtering etc.
		# background substraction
		IJ.run(imp, "Subtract Background...", "rolling="+str(backgroundRadius)+" slice")
		# sharpen endges of remaining structures, 
		IJ.run(imp, "Unsharp Mask...", "radius="+str(sharpeningRadius)+" mask=0.90 slice")
		# perform reduction using threshold
		IJ.run(imp, "Make Binary", "slice")
		# reduce noise
		IJ.run(imp, "Despeckle", "slice")
		
		
		pa.analyze(imp)
		# col: colName ( get it with patable.getColumnHeading(int)) 
		# 0: Area, 1: Mean, 2: StdDev, 3: Mode, 4: Min, 5: Max, 6: X, 7: Y, 8: XM, 9: YM, 10: Perim.
		# 11: BX, 12: BY, 13: Width, 14: Height, 15: Major, 16: Minor, 17: Angle, 18: Circ., 19: Feret,
		# 20: IntDen, 21: Median, 22: Skew, 23: Kurt, 24: %Area, 25: RawIntDen, ...
		
		# duplicate current binary image
		imppart = imp.duplicate()
		imppart.show()
		
		# get skeleton from image (structures are thinned until wideness of one pixel is reached) 
		IJ.run(imp, "Skeletonize (2D/3D)", "slice")
		# Analyze skeleton
		# run(int pruneIndex, boolean pruneEnds, boolean shortPath, ImagePlus origIP, boolean silent, boolean verbose)
		skelResult = skel.run(AnalyzeSkeleton_.NONE, False, True, None, True, False)
		
		######## Perform Particle Analysis
		# Read Results
		AR = patable.getColumn(33) 		  # Aspect Ratio
		Perimeter = patable.getColumn(10) # Perimeter
		Major = patable.getColumn(15)	  # major axis	
		
		# Calculating Averages & Distributions
		AvAspectRatio = 0
		AvPerimeter = 0
		AvMajor = 0
		DistAspectRatio = [0,0,0,0]
		DistPerimeter = [0,0,0,0]
		DistMajor = [0,0,0,0]
		for particle in range(len(AR)):
			AvAspectRatio += AR[particle]
			AvPerimeter += Perimeter[particle]
			AvMajor += Major[particle]
	
			if AR[particle] <= 1.3:
				DistAspectRatio[0] += 1
			elif AR[particle] <=  1.6:
				DistAspectRatio[1] += 1
			elif AR[particle] <= 2.3:
				DistAspectRatio[2] += 1
			else:
				DistAspectRatio[3] += 1
	
			if Perimeter[particle] <= 1:
				DistPerimeter[0] += 1
			elif Perimeter[particle] <=  2:
				DistPerimeter[1] += 1
			elif Perimeter[particle] <= 5:
				DistPerimeter[2] += 1
			else:
				DistPerimeter[3] += 1
				
			if Major[particle] <= 1:
				DistMajor[0] += 1	
			elif Major[particle] <= 3:
				DistMajor[1] += 1	
			elif Major[particle] <= 5:
				DistMajor[2] += 1	
			else:
				DistMajor[3] += 1	
		AvAspectRatio = AvAspectRatio/len(AR)
		AvPerimeter = AvPerimeter/len(AR)
		AvMajor = AvMajor/len(AR)
		
				
		######### Perform Skeleton Analysis
		# Read results
		branchLengths = skelResult.getAverageBranchLength()
		branchNumbers = skelResult.getBranches()
		numTrees = skelResult.getNumOfTrees()
		junctions = skelResult.getJunctions()
		# Calculating Averages & Distributions
		# calculate total length (average branch length * branch numbers)
		totalLength = 0
		totJunctions = 0
		totalBranchNr = 0
		DistJunction = [0,0,0,0]
		DistLength = [0,0,0,0]
		DistBranch = [0,0,0,0]
		for i in range( len( branchNumbers)):
			totalLength += branchNumbers[i] * branchLengths[i]
			totJunctions += junctions[i]
			totalBranchNr += branchNumbers[i]
		
			if branchNumbers[i] * branchLengths[i] <= 0.5:
				DistLength[0] += 1
			elif branchNumbers[i] * branchLengths[i] <= 1:
				DistLength[1] +=1
			elif branchNumbers[i] * branchLengths[i] <= 2:
				DistLength[2] +=1  
			else:
				DistLength[3] +=1	
				
			if branchNumbers[i] == 0:
				DistBranch[0] += 1
			elif branchNumbers[i] <= 2:
				DistBranch[1] += 1
			elif branchNumbers[i] == 5:
				DistBranch[2] += 1
			else:
				DistBranch[3] += 1	
							 
			if junctions[i] == 0:
				DistJunction[0] += 1
			elif junctions[i] == 1:
				DistJunction[1] +=1
			elif junctions[i] == 2:
				DistJunction[2] +=1  
			else:
				DistJunction[3] +=1 
		###########################################################
		# save results in one variable
			
		results[roi][1] = len(branchNumbers) # number of trees
		results[roi][2] = totalLength		 # total length of all trees
		results[roi][3] = DistLength[0]		 # distr of tree length
		results[roi][4] = DistLength[1]
		results[roi][5] = DistLength[2]
		results[roi][6] = DistLength[3]
		results[roi][7] = totalBranchNr		 # total number of branches
		results[roi][8] = totalBranchNr/float(numTrees) # branches per tree
		results[roi][9] = DistBranch[0]		 # distr of branches in each tree
		results[roi][10] = DistBranch[1]
		results[roi][11] = DistBranch[2]
		results[roi][12] = DistBranch[3]
		results[roi][13] = totJunctions		 # number of total junctions
		results[roi][14] = DistJunction[0]	 # distr of junction count in each tree
		results[roi][15] = DistJunction[1]
		results[roi][16] = DistJunction[2]
		results[roi][17] = DistJunction[3]
		results[roi][18] = AvPerimeter		 # Average Perimeter
		results[roi][19] = DistPerimeter[0]	 # distr of perimeters
		results[roi][20] = DistPerimeter[1]	
		results[roi][21] = DistPerimeter[2]	
		results[roi][22] = DistPerimeter[3]	
		results[roi][23] = AvAspectRatio	 # average Aspect Ratio
		results[roi][24] = DistAspectRatio[0] # distr aspect ratio
		results[roi][25] = DistAspectRatio[1]
		results[roi][26] = DistAspectRatio[2]
		results[roi][27] = DistAspectRatio[3]
		results[roi][28] = AvMajor 			# average Major Axis
		results[roi][29] = DistMajor[0]		# distr Major Axis
		results[roi][30] = DistMajor[1]
		results[roi][31] = DistMajor[2]
		results[roi][32] = DistMajor[3]
		
		
		# closes duplicate image w/o asking for saving
		imppart.changes = False
		imp.changes = False
		imppart.close()
		imp.close()
		
	for roi in range(len(roi_list)):
		imp1.setRoi(roi_list[roi])
		stats = imp1.getStatistics()
		results[roi][0] = stats.area
	
		# save result extended to ResultTable
		myfile = open(outputDir+"00_Results_extended.csv", "a")
		myfile.write(filename+"\t"+str(roi+1)+"\t")
		for i in range(33):
			myfile.write(str(results[roi][i])+"\t")
		myfile.write("\n")
		myfile.close()

###########################################################################3
########### 		MAIN

# variables to open several images at once (in total 3) in order to compare		
imp2 = 0
imp3 = 0

RoiManager()  # starts RoiManager
roi_manager = RoiManager.getInstance()

# prompts to get input and output directory
inputDir = DirectoryChooser("Please choose a directory to get images from.").getDirectory()
outputDir = DirectoryChooser("Please choose a directory to save results.").getDirectory()

if (not inputDir) | (not outputDir):
	# user canceled dialog
	pass


else:

	# create file to write results to
	myfile = open(outputDir + "00_Results_extended.csv", "w")
	myfile.write("FILENAME\tROI IND\tAREA\tNUM_TREES\t TOT_LENGTH\t<=0.5\t<=1\t<=2\t>2\t ")
	myfile.close()
	myfile = open(outputDir + "00_Results_extended.csv", "a")
	myfile.write("NUM_BRANCHES\t BRANCH_PER_TREE \t=0\t<=2\t<=5\t>5\t")
	myfile.write("NUM_JUNCTS\t=0\t=1\t=2\t>2\t")
	myfile.write("AV PERIMETER\t<=1.5\t<=5\t<=10\t>10\t")
	myfile.write("AV AR\t <=1.3\t<=1.6\t<=2.3\t>2.3\t")
	myfile.write("AV MAJOR\t <=1\t<=3\t<=5\t>5\n")
	myfile.close()

	# initialize AnalyzeSkeleton
	skel = AnalyzeSkeleton_()
	skel.calculateShortestPath = True
		
	# loop over all files in output directory
	for root, dirs, files in os.walk(inputDir,topdown = False):
		for fileName in sorted(files):
			# open only files with ending lsm
			if fileName.split(".")[-1] == "lsm":  
			
				# open file & display
				imps = BF.openImagePlus(os.path.join(inputDir, fileName))
				for imp in imps:
					imp.show()
				# get filename for saving
				name=imp.getShortTitle()

				# delete last ROIs
				roi_manager.reset()
				# record current ROIs from user imput
				WaitForUserDialog("Action required", "Please mark each cell to be analyzed with a ROI: \nSurround each cell with the Freehand-Tool and press 't'. \nPress OK when done.").show()
				# calibrate parameter for filtering
				pixelwidth = imp.getCalibration().pixelWidth
				backgroundRInPixel = backgroundRadius/pixelwidth
				sharpeningRInPixel = sharpeningRadius/pixelwidth
				# analyze each ROI = cell
				if len(roi_manager.getRoisAsArray()) != 0:
					AnalyzeROIs(imp, roi_manager, name, backgroundRInPixel, sharpeningRInPixel)
				# show all ROIs in order to save them 
				roi_manager.runCommand("Show All")
				# save image plus ROIs as tif into output directory
				IJ.save(imp, os.path.join(outputDir, name + "-ROI.tif"))
				# close third-last image
				if imp3 != 0:
					imp3.close()
				if imp2 != 0:
					imp3 = imp2
				imp2 = imp
	# close all open images
	if imp2 != 0:
		imp2.close()
	if imp3 != 0:
		imp3.close()




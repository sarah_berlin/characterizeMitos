# Characterizing mitochondria in cells from microscope images – 
# fused or fragmented?
a program written by Sarah Lück
sarah.lueck@hu-berlin.de

## What?

- 1 main program (**_characterizeMitos.py_**) which characterize marked cells on a microscope image  and automatically decide if these cells contain rather fused or fragmented mitochondria. 

- 2 additional programs to test image filtering steps (**_testFilter.py_**) and read out different quantities (**_extendedCharacterizeMitos.py_**) in order to adjust the main program according to your needs. This is necessary since mitochondria might look different in different cell types or quality of images may need different filtering strengths.

## How to use it?

All of these programs are scripts for Fiji, a further development of ImageJ, which can be downloaded [here](http://fiji.sc/).

### Starting any of these programs in Fiji (ImageJ):
Main Toolbar:
- File > New > Script...
A window pops up: File> Open > choose from dialog the program you want to run.
Once it is loaded into the window, press “Run”.

### characterizeMitos.py

Following steps will pop up:
1. Choose a directory with images to be analyzed.
2. Choose a directory to save results. It can be the same as the first one.
3. The ROI-manager pops up.
4. The program iterates through the directory. Each image with the ending .lsm is opened and one is asked in a pop-up-window to mark (surround) each cell with the freehand tool on the image and afterwards to press “T” (further information below). Once every cell one wants to analyze is marked (also no marked cells are possible) one hits the OK button in the pop-up-window. 
5. After hitting the OK-button the program analyzes each cell separately and writes the result into the result file: 00_results.txt. Additionally a copy of the analyzed image together with the marked cells is saved: PreviousFilename-ROI.tif. Both files are saved to the directory specified in 2nd.
The next image is opened and the program waits for another input.
The program will keep the last two images open which hopefully helps to keep track of the already marked cells. 
6. Once every image has been processed a summary file is written: 00_summary.txt.  

#### Image handling while marking the cells
When surrounding the cell one actually produces a “Region Of Interest” (ROI). Pressing “T” saves it to the ROI-manager. If a region (cell) is not saved to the ROI-manager it will not be analyzed. To surround a cell any tool (cp. Fig. 1) can be used but I find the Freehand-Tool most convenient. Clicking outside of a ROI before saving it to the manager will delete the ROI. 
Images can be enlarged by pressing Ctrl+using the mouse wheel.


Figure 1: Tools to mark ROIs. The freehand tool is selected in the figure.


#### Checking results in .tif-image

In any result table the file name together with the ROI index is noted for every analyzed cell. To check if mitochondria are characterized correctly one should know which result from the result table belongs to which cell (ROI) in the original image. To load previously saved ROIs one opens the tif-image produced by characterizeMitos.py or extendedCharacterizeMitos.py together with the ROI-manager (Analyze > Tools > ROI Manager...). Please delete any entries in the ROI-manager. To transfer ROIs from the tif-image to the ROI-manager one clicks: Image > Overlay > To ROI Manager.

### testFilter.py

This program processes an already open image. The program performs the filtering steps which are used in characterizeMitos.py. The result consists of two images which are the base for the quantitative analysis performed by characterizeMitos.py. Further information below.

### extendedCharacterizeMitos.py

This program works and processes the images exactly the same as characterizeMitos.py. But instead of saving a 00_results.txt it saves a more detailed analysis without the final characterizing step into 00_extended_results.csv. This large table can be opened by any spreadsheet e.g. Excel.


## How are cells categorized?

The process of deciding if a cells contains fused or fragmented mitochondria splits up into to parts. First the image is processed with different filtering steps in order to bring out the mitochondria structure. The structures are then analyzed.

### From original image to filtered image.
Following steps are done
1. Background substraction:
The method used here is called “Rolling Ball”. Imagine a 3D surface with the pixel values of the image being the height, then a ball rolling over the back side of the surface creates the background which is then substracted. The parameter which defines the background substraction is the ball radius. A ball with a large radius deletes an uneven plane, a small radius can separate structures, cp. Figure 2.

2. Sharpening:
Edges of remaining structures are enhanced. As in step before the filter is defined by a radius. A large radius enlarges structures, cp. Figure 3. 


Figure 2: Background substraction using the Rolling Ball method. Below each image intensity histograms for the marked regions are shown. A large ball radius reduces only an uneven background. A smaller ball radius can additionally separate structures. 

Figure 3: Illustration of Sharpening.  Edges of structures are enhanced. A large sharpening radius also increases structures.

3. Converting image to Black-White (binary) image using a histogram-derived thresholding methods.
4. Reducing noise (despeckling).
5. Skeletonizing, structures are thinned until one-pixel width.

With these filtering steps two images are created, one with structures reflecting  the circumferences of the mitochondria as so-called particles, a second where mitochondria are represented by a skeleton or tree-like structure, cp. Figure 4 at the end of the manuscript.

### Extracting quantities from filtered images.
Both images are now analyzed by extracting characteristics described by numbers, e.g. perimeter, branch number etc.. For all characteristics an average is calculated which form then the base for a decision if cell contains fused or fragmented mitochondria. An illustration of characteristics can be found in Figure 5.

I decided to give scores for a either fragmented or fused phenotype. The maximal reachable score is   10 points. The mitochondria of a cell are noted as fragmented or fused when the respective category   has a score of 4 or more. I decided for the implemented thresholds after I analyzed in each category 19 cells with a clear phenotype.



#### Analyzed characteristics

Characteristic 
What to expect?
Threshold for fused mitochondria
Threshold for fragmented mitochondria
average perimeter
(particles)
Fused mitochondria form large structures with a large perimeter.
> 4: score_fused: +1
> 5: score_fused: +2
> 6: score_fused: +3
< 4: score_frag: +1
< 3.8: score_frag: +2
average aspect ratio
(particles)
Fused mitochondria have an elongated shape and therefore a larger aspect ratio.
> 2.1: score_fused: +1
< 2: score_frag: +1
branches per tree
(skeleton)
Fused mitochondria have a many trees with a lot of branches. There are trees, i.e. tiny structures,  possible with zero branches.
> 1.1: score_fused: +1
> 1.2: score_fused: +2
< 1.0: score_frag: +1
< 0.9: score_frag: +2

How to adjust the script to your needs?

I highly recommend to check if the program distinguishes fused and fragmented mitochondria correctly. This can vary from cell type to cell type as mitochondria look different in different cell types. Additionally, the quality of the image, e.g. a weak contrast, influences the outcome. 

If the program does not distinguish mitochondrial shapes correctly it is very likely that the filtered images do not reflect the mitochondrial phenotype correctly. To check the filtering steps please use the program testFilter.py (cp. section How to use it?) together with a few original images. Since the filtering rather destroys connections check if elongated and fused mitochondria still appear as connected structures or single trees in the filtered images. If this is not case one can change the filtering parameters “background radius” and “sharpening radius” (cp. section How are cells categorized?, Figure 2, 3). 
They are noted in the beginning of each program. The program (script) can be edited in the window which is used to run the program. Try out different values by changing theses lines, saving and rerunning the program.
If you are satisfied with the result, please change the parameters also in the main program characterizeMitos.py 

If the filtered images look good but the mitochondria are still not categorized correctly the analysis does not work properly. Then it is necessary to implement other thresholds or other quantities should be analyzed. A hint could give the program extendedCharacterizeMitos.py. This program produces  an extended result table where more quantities are analyzed than in the main program. The goal is to find quantities together with thresholds which distinguish both phenotypes. These new quantities have to be then written into the main program.


In the extended result table there is often an average quantity together with four absolute values for certain ranges. 
<= … smaller or equal

Content of the extended result table:

- Area of whole ROI
From skeleton image:
- number of trees
- total length of all trees together
- number of trees with length <= 0.5, > 0.5 and <= 1, > 1 and <= 2, > 2
- total number of branches
- number of trees with branch number 0, <=2, <=5, >5
- total number of junctions
- trees with junction number 0, 1, 2, >2
From particle image:
- average perimeter
- number of particles with perimeter <=1.5,  >1.5 and <=5, >5 and <=10, >10
- average aspect ratio
- number of particles with aspect ratio <=1.3, >1.3 and <=1.6, >1.6 and <=2.3, >2.3
- average major axis
- number of particles with major axis <=1, >1 and <=3, >3 and <=5, >5

In principle the extended result table can be even more extended. Especially the particle image can be analyzed in many different ways. A complete list of quantities can be found here:
https://imagej.nih.gov/ij/docs/guide/146-30.html (30.7 Set Measurements...)

A list of the quantites which characterize the skeleton can be found here:
http://imagej.net/AnalyzeSkeleton



Figure 5: Illustration of measured quantities. A Shown are one particle image and the respective perimeter in red. For measuring the aspect ratio and the major axis, an ellipse for each structure is constructed. The aspect ratio is then the ratio between major axis, i.e. longest diameter, and minor axis. B The skeleton image  for the previous particle image is shown together with its measurable quantities. A tree is one single structure, hence the image contains three trees. Each tree can consist of  several branches. In each structure one brnach is marked in red. The largest structure consists of 5 branches. The smallest structure only consisting of one pixel has no branch. Junctions connect 3 or more branches. 



Figure 4.1: Original image.






Figure 4.2: Particle image after filtering steps 1-4 (cp. section How are cells categorized?)





Figure 4.3: Skeleton image. The particle image is additionaly skeletonized.


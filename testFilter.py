#### SET FILTERING PARAMETERS #####
#
# parameters of background substraction (radius) and sharpening (radius)
# can be changed to influence outcome
#	
# ### background substraction: ###
# Everything gets substracted, where a ball with certain size (e.g. radius=20 pixel) can reach
# Structures larger than this radius will vanish.
#
# ### sharpening ###
# Exaggerate edges of remaining structures after background substraction 
# 2 parameters: 
# 	radius: influences thickness of resulting structure
# 	mask: "steepness" of edges, i.e. contrast between structure and background,
# 		  highest possible value (=0.9) recommended 

<<<<<<< HEAD
backgroundRadius = 6 # radius in microns
sharpeningRadius = 0.7  # radius in microns	
=======
backgroundRadius = 1.2  # radius in microns
sharpeningRadius = 5  # radius in pixel	
>>>>>>> e0679a883217f698edffce1cca550e60c8283adf

from ij import IJ


imp = IJ.getImage()
IJ.run(imp, "Duplicate...", "name=")
imp = IJ.getImage()
pixelwidth = imp.getCalibration().pixelWidth

# background substraction
IJ.run(imp, "Subtract Background...", "rolling="+str(backgroundRadius/pixelwidth)+" slice")
# sharpen endges of remaining structures, 
IJ.run(imp, "Unsharp Mask...", "radius="+str(sharpeningRadius/pixelwidth)+" mask=0.90 slice")
# perform reduction using threshold
IJ.run(imp, "Auto Threshold", "method=IsoData white")
# Binary: only black and white
IJ.run(imp, "Make Binary", "slice")
# reduce noise
IJ.run(imp, "Despeckle", "slice")
# duplicate once more
IJ.run(imp, "Duplicate...", "name=")
imp1 = IJ.getImage()
# get skeleton from image (structures are thinned until wideness of one pixel is reached) 
IJ.run(imp1, "Skeletonize (2D/3D)", "slice")
<<<<<<< HEAD
=======

imp.changes = False
imp1.changes = False
>>>>>>> e0679a883217f698edffce1cca550e60c8283adf
